package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void planetContainsObstaclesInput() throws MarsRoverException {
		List<String> planetObstacles = List.of("(2,3)","(5,7)");
		MarsRover rover;

		rover = new MarsRover(10, 10, planetObstacles);

		assertTrue(rover.planetContainsObstacleAt(2, 3));
	}

	@Test
	public void commandStringEmptyShouldReturnSamePosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);

		assertTrue(rover.executeCommand("").equals("(0,0,N)"));
	}

	@Test
	public void commandStringTurnLeftShouldReturnSamePositionDifferentDirection() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover;
				rover = new MarsRover(10, 10, planetObstacles);

		assertTrue(rover.executeCommand("l").equals("(0,0,W)"));
	}

	@Test
	public void commandStringTurnRigthShouldReturnSamePositionDifferentDirection() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);

		assertTrue(rover.executeCommand("r").equals("(0,0,E)"));
	}

	@Test
	public void commandStringForwardShouldReturnSameDirectionDifferentPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);

		assertTrue(rover.executeCommand("f").equals("(0,1,N)"));
	}

	@Test
	public void commandStringBackwardShouldReturnSameDirectionDifferentPosition() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("r");
		rover.executeCommand("r");
		assertTrue(rover.executeCommand("b").equals("(0,1,S)"));
	}

	@Test
	public void commandStringCombinedMoving() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.executeCommand("ffrff").equals("(2,2,E)"));
	}
	
	@Test
	public void commandStringWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.executeCommand("b").equals("(0,9,N)"));
	}
	
	@Test
	public void singleObstacle() throws MarsRoverException {
		List<String> planetObstacles = List.of("(2,2)");
		MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.executeCommand("ffrfff").equals("(1,2,E)(2,2)"));
	}
	
	@Test
	public void multipleObstacle() throws MarsRoverException {
		List<String> planetObstacles = List.of("(2,2)","(2,1)");
		MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.executeCommand("ffrfffrflf").equals("(1,1,E)(2,2)(2,1)"));
	}
	
	@Test
	public void wrappingAndObtacle() throws MarsRoverException {
		List<String> planetObstacles = List.of("(0,9)");
		MarsRover rover;
		rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.executeCommand("b").equals("(0,0,N)(0,9)"));
	}


}
