package tdd.training.mra;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MarsRover {
	int planetX, planetY;
	List<String> planetObstacles;
	String roverDirection;
	int xRoverPosition, yRoverPosition;
	int xObstacle, yObstacle;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.xRoverPosition = 0;
		this.yRoverPosition = 0;
		this.roverDirection = "N";

	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		String[] position;
		int positionX, positionY;
		
		for (int i = 0; i < this.planetObstacles.size(); i++) {
			position = this.planetObstacles.get(i).split(",");

			positionX = Integer.parseInt(position[0].substring(1));
			positionY = Integer.parseInt(position[1].substring(0, position[1].length() - 1));

			if (x == positionX && y == positionY) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		Set <String> obstacle = new LinkedHashSet<>() ;
		String ris ="";
		for (int i = 0; i < commandString.length(); i++) {
			this.turn(commandString.charAt(i));
			this.move(commandString.charAt(i));
			
			
			if (this.planetContainsObstacleAt(xRoverPosition, yRoverPosition)) {
				obstacle.add( this.format(xRoverPosition, yRoverPosition));
				this.undo(commandString.charAt(i));
				}
		}
		ris =this.format(xRoverPosition, yRoverPosition, roverDirection);
		for(String s: obstacle)
			ris += s;
		return ris;	
		}

	public String format(int x, int y, String dir) {
		return "(" + x + "," + y + "," + dir + ")";
	}

	public String format(int x, int y) {
		return "(" + x + "," + y + ")";
	}

	public void move(char commandChar) {
		if (commandChar == 'f')
			switch (this.roverDirection) {
			case "W":

				if (this.xRoverPosition == 0)
					this.xRoverPosition = 9;
				else
					this.xRoverPosition -= 1;

				break;

			case "E":

				if (this.xRoverPosition == 9)
					this.xRoverPosition = 0;
				else
					this.xRoverPosition += 1;

				break;
			case "S":
				if (this.yRoverPosition == 0)
					this.yRoverPosition = 9;
				else
					this.yRoverPosition -= 1;
				break;
			case "N":
				if (this.yRoverPosition == 9)
					this.yRoverPosition = 0;
				else
					this.yRoverPosition += 1;
			}

		if (commandChar == ('b'))
			switch (this.roverDirection) {
			case "W":
				if (this.xRoverPosition == 9)
					this.xRoverPosition = 0;
				else
					this.xRoverPosition += 1;
				break;
			case "E":
				if (this.xRoverPosition == 0)
					this.xRoverPosition = 9;
				else
					this.xRoverPosition -= 1;
				break;
			case "S":
				if (this.yRoverPosition == 9)
					this.yRoverPosition = 0;
				else
					this.yRoverPosition += 1;
				break;
			case "N":
				if (this.yRoverPosition == 0)
					this.yRoverPosition = 9;
				else
					this.yRoverPosition -= 1;

			}
	}

	public void undo(char commandChar) {
		char command = commandChar;
		if (commandChar == 'f')
			command= 'b';

		if (commandChar == 'b')
			command = 'f';

		if (command == 'f')
			switch (this.roverDirection) {
			case "W":

				if (this.xRoverPosition == 0)
					this.xRoverPosition = 9;
				else
					this.xRoverPosition -= 1;

				break;

			case "E":

				if (this.xRoverPosition == 9)
					this.xRoverPosition = 0;
				else
					this.xRoverPosition += 1;

				break;
			case "S":
				if (this.yRoverPosition == 0)
					this.yRoverPosition = 9;
				else
					this.yRoverPosition -= 1;
				break;
			case "N":
				if (this.yRoverPosition == 9)
					this.yRoverPosition = 0;
				else
					this.yRoverPosition += 1;
			}

		if (command == ('b'))
			switch (this.roverDirection) {
			case "W":
				if (this.xRoverPosition == 9)
					this.xRoverPosition = 0;
				else
					this.xRoverPosition += 1;
				break;
			case "E":
				if (this.xRoverPosition == 0)
					this.xRoverPosition = 9;
				else
					this.xRoverPosition -= 1;
				break;
			case "S":
				if (this.yRoverPosition == 9)
					this.yRoverPosition = 0;
				else
					this.yRoverPosition += 1;
				break;
			case "N":
				if (this.yRoverPosition == 0)
					this.yRoverPosition = 9;
				else
					this.yRoverPosition -= 1;

			}
	}

	public void turn(char commandChar) {
		if (commandChar == 'l')
			switch (this.roverDirection) {
			case "W":
				this.roverDirection = "S";
				break;
			case "E":
				this.roverDirection = "N";
				break;
			case "S":
				this.roverDirection = "E";
				break;
			case "N":
				this.roverDirection = "W";

			}
		if (commandChar == 'r')
			switch (this.roverDirection) {
			case "W":
				this.roverDirection = "N";
				break;
			case "E":
				this.roverDirection = "S";
				break;
			case "S":
				this.roverDirection = "W";
				break;
			case "N":
				this.roverDirection = "E";
			}
	}

}
